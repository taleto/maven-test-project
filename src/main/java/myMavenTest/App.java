package myMavenTest;

import java.util.List;

public class App {

	public static void main(String[] args) {
		Db ourDataBase = new Db();
		StudentDAOImpl studentRepository = new StudentDAOImpl(ourDataBase);
		studentRepository.insert(new Student(1, "Aleksandar", "Talevski"));
		studentRepository.insert(new Student(2, "Marjan", "Marjanovski"));
		List<Student> allStudents = studentRepository.findAll();
		for (int i = 0; i < allStudents.size(); i++) {
			Student foundStudent = studentRepository.findById(allStudents.get(i).getId());
			System.out.println(
					foundStudent.getId() + " " + foundStudent.getFirstName() + " " + foundStudent.getLastName());
		}
		studentRepository.update(new Student(2, "Marjan", "Nikolovski"));
		studentRepository.deleteById(1);
		for (int i = 0; i < allStudents.size(); i++) {
			Student foundStudent = studentRepository.findById(allStudents.get(i).getId());
			System.out.println(
					foundStudent.getId() + " " + foundStudent.getFirstName() + " " + foundStudent.getLastName());
		}
	}

}
