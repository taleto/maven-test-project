package myMavenTest;

import java.util.List;

public class StudentDAOImpl implements StudentDAO {
	private Db DataBase;

	public Student findById(Integer id) {
		Student result = null;
		for (int i = 0; i < DataBase.getStudents().size(); i++) {
			Student currentStudent = DataBase.getStudents().get(i);
			if (currentStudent.getId() == id) {
				return currentStudent;

			}
		}	
			return result;

	}

	public List<Student> findAll() {
		return DataBase.getStudents();
	}

	public int insert(Student student) {
			if (student == null) {
				return 0;
			}
			boolean inserted = DataBase.getStudents().add(student);
			if (inserted) {
			return	DataBase.getStudents().get(DataBase.getStudents().size()-1).getId();
			}
			return 0;
	}

	public int deleteById(Integer id) {
		 
		if (id == null) {
			return 0;
		}
		Student affected = this.findById(id);
		if (affected != null) {
		int removeOnIndex =	DataBase.getStudents().indexOf(affected);
		DataBase.getStudents().remove(removeOnIndex);
		return affected.getId();
		}
		return 0;
	}

	public int update(Student student) {
		if (student == null) {
			return 0;
		}
		Student studentToUpdate = this.findById(student.getId());
		if (studentToUpdate != null) {
			studentToUpdate.setFirstName(student.getFirstName());
			studentToUpdate.setLastName(student.getLastName());
			return studentToUpdate.getId();
		}
			return 0;
	}

	public StudentDAOImpl(Db dataBase) {
		DataBase = dataBase;
	}

}
